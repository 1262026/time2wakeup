import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  bg: {
      flex: 1,
      backgroundColor: '#d7dbe0',
  },
  container: {
    flex: 1
  },
  fontBold: {
    fontWeight: 'bold',
  },
  colorBlack: {
    color: '#000'
  },
  flexDirectionRow: {
    flexDirection: 'row'
  },
  justifyContentSpace: {    
    justifyContent: 'space-between',
  },
  textAlignLeft: {
    textAlign: 'left'
  },
  textAlignRight: {
    textAlign: 'right'
  },
  fontSize15: {
    fontSize: 15
  },
  fontSize16: {
    fontSize: 16
  }
});