import React from 'react';
import { Text, View } from 'react-native';
import { createBottomTabNavigator } from 'react-navigation';
import { YellowBox } from 'react-native'

import HomeScreen from "./Views/home.js"
import SettingsScreen from "./Views/settings.js"
import About from "./Views/About/aboutIndex.js"

export default createBottomTabNavigator({
  'Hẹn giờ': HomeScreen,
  'Giới thiệu': About,
  'Chức năng': SettingsScreen,
});

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated'])