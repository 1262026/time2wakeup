import React from 'react';
import { Text, View, TouchableOpacity, Alert, StyleSheet, Image, Button  } from 'react-native';
import styles from './../../lib/layoutStyle.js'

export default class About extends React.Component {
    static navigationOptions = {
        title: 'Giới thiệu',
      };
    onPress = () => {
        Alert.alert('You tapped the button!')
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.bg}>
                <View style={[st.aboutImageTop]}>
                    <Image source={require('./../../Content/about.png')} />
                    <View style={{marginLeft: 10, paddingTop: 20}}>
                        <Text style={[styles.fontBold, st.titlePro, styles.colorBlack]}>Time to Wakeup</Text>
                        <Text style={styles.fontBold}>© 2018 LĐ</Text>
                    </View>
                </View>
                <Text style={{marginLeft: 10}}>Ứng dụng</Text>
                <View style={st.groupDiv}>
                    <View style={[styles.flexDirectionRow, st.borderbottom, styles.justifyContentSpace, st.rowInfo]}>
                        <Text style={[styles.colorBlack, styles.fontSize15, styles.fontBold]}>Tác giả</Text>
                        <Text style={[styles.fontSize15]}>Luân Đình</Text>
                    </View>
                    <View style={[styles.flexDirectionRow, st.borderbottom, styles.justifyContentSpace, st.rowInfo]}>
                        <Text style={[styles.colorBlack, styles.fontSize15, styles.fontBold]}>Phiên bản</Text>
                        <Text style={[styles.fontSize15]}>Beta 1</Text>
                    </View>
                </View>
                <Text style={{marginLeft: 10, marginTop: 15}}>Ủng hộ</Text>
                <View style={st.groupDiv}>
                    <TouchableOpacity style={[st.btn, styles.flexDirectionRow, styles.justifyContentSpace, st.rowInfo]} onPress={() => navigate('Chat')}>
                        <Text style={[styles.colorBlack, styles.fontSize15, styles.fontBold]}>Tặng 1 ly cafe</Text>
                        <Text style={[styles.colorBlack, styles.fontSize15]}>></Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const st = StyleSheet.create({
    groupDiv:{
        backgroundColor: '#fff',
        marginTop: 10,
        borderWidth: 1,
        borderColor: '#b2b2b3'
    },
    aboutImageTop: {
        marginTop: 25,
        flexDirection:'row',
        justifyContent:'center'
    },
    btn: {
        padding: 10
    },
    titlePro: {
        fontSize: 25,
    },
    rowInfo: {
        padding: 10
    },
    borderbottom: {
        borderBottomWidth: 1,
        borderBottomColor: '#b2b2b3'
    }
});