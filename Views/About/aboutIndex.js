import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  Button,
  View,
  Navigator,
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import About from "./about.js";

class ChatScreen extends React.Component {
  static navigationOptions = {
    title: 'Chat with Lucy',
  };
  render() {
    return (
        <View>
          <Text>Chat with Lucy</Text>
        </View>
    );
  }
}

const App1 = StackNavigator({
  Home: { screen: About },
  Chat: { screen: ChatScreen },
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default App1